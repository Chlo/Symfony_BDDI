<?php
/**
 * Created by PhpStorm.
 * User: Chloé
 * Date: 27/11/2017
 * Time: 15:18
 */

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class CourseController extends Controller
{
    /**
     * @Route("/course/list", name="course_list")
     */
    public function listAction()
    {
        return $this->render('course/list.html.twig');
    }

    public function searchAction()
    {
        return $this->render('course/search.html.twig');
    }
}